import cv2

img = cv2.imread('LaboratorioGluon.jpg')
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
mask = cv2.inRange(hsv, (15, 0, 0), (30, 255, 255))

cnts, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[-2:]

for contour in cnts:
    cv2.drawContours(img, contour, -1, (0, 255, 0), 2)

cv2.imshow("Mascara", mask)
cv2.imshow("Imagen", img)
cv2.waitKey(0)
cv2.destroyAllWindows()
