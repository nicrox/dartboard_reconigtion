import cv2
img = cv2.imread('LaboratorioGluon.jpg')
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
mask = cv2.inRange(hsv, (15,0,0), (30,255,255))
cv2.imshow("Mascara", mask)
cv2.imshow("Imagen", img)
cv2.waitKey(0)
