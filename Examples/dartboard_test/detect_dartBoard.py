import cv2
import numpy as np 

img = cv2.imread('D:\Oproyects\PROYECTOS\dartboard_reconigtion\Examples\dartboard_test\cAMDX.png')
img_thresholded = cv2.inRange(img, (60, 60, 60), (140, 140, 140))

kernel = np.ones((10,10),np.uint8)
opening = cv2.morphologyEx(img_thresholded, cv2.MORPH_OPEN, kernel)

contours, hierarchy = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
#print len(contours)
cv2.imshow("Imagen-1", img_thresholded)
cv2.waitKey(0)

i=1
for contour in contours:
    (x,y),radius = cv2.minEnclosingCircle(contour)
    center = (int(x),int(y))
    radius = int(radius)
    cv2.circle(img,center,radius,(0,255,0),2)
    # labelling the circles around the centers, in no particular order.
    position = (center[0] - 10, center[1] + 10)
    text_color = (0, 0, 255)
    cv2.putText(img, str(i), position, cv2.FONT_HERSHEY_SIMPLEX, 1, text_color, 3)
    i=i+1

cv2.imshow("Imagen-2", img)
cv2.waitKey(0)